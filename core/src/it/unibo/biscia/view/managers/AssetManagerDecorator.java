package it.unibo.biscia.view.managers;

/**
 * A decorator for the asset manager.
 *
 */
public interface AssetManagerDecorator {

    /**
     * Builds font's parameters and loads it.
     * 
     * @param fontAsset The asset of type FontManager.Font
     */
    void loadFont(Asset<FontManager.Font> fontAsset);

    /**
     * Build's a skin with its resources and loads it.
     * 
     * @param skinAsset The asset of type SkinManager.Skin
     */
    void loadSkin(Asset<SkinManager.Skin> skinAsset);

    /**
     * Loads a sound Asset.
     * 
     * @param soundAsset the sound asset
     */
    void loadSound(Asset<SoundManager.Sound> soundAsset);

    /**
     * Loads a music asset.
     * 
     * @param musicAsset the music asset
     */
    void loadMusic(Asset<MusicManager.Music> musicAsset);

}
