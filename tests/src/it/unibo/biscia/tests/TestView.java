package it.unibo.biscia.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;

import it.unibo.biscia.core.Controller;
import it.unibo.biscia.core.ControllerImpl;
import it.unibo.biscia.core.Direction;
import it.unibo.biscia.events.ActionObserver;
import it.unibo.biscia.events.GenericEventSubjectImpl;
import it.unibo.biscia.view.managers.AssetManagerDecoratorImpl;
import it.unibo.biscia.view.managers.SoundManager;
import it.unibo.biscia.view.utils.PlayerOneProcessor;
import it.unibo.biscia.view.utils.PlayerProcessorImpl;
import it.unibo.biscia.view.utils.PlayerTwoProcessor;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

/**
 * Automated tests for the View package
 *
 */
@RunWith(GdxTestRunner.class)
public class TestView {
    
    /**
     * {@link PlayerProcessorImpl} test. It tests the typical scenario of successfully handling the right player's keys and firing them.
     */
    @Test
    public void testPlayerProcessor() {
        //Creates a Mock object of the actionObserver
        ActionObserver actionObserver = mock(ActionObserver.class);
        //create the Controller to instantiate the players
        Controller controller = new ControllerImpl(Arrays.asList("player1", "player2"), Controller.Speed.SPEED5, false);
        //Instantiate the actionSubject and bind the actionObserver to it
        GenericEventSubjectImpl<ActionObserver> actionSubject = new GenericEventSubjectImpl<>();  
        actionSubject.attach(actionObserver);
        //instantiate the player processor and simulate a right arrow key press on player1 and left arrow key press on player2
        PlayerOneProcessor player1Processor = new PlayerOneProcessor(controller.getPlayers().get(0), actionSubject);
        PlayerTwoProcessor player2Processor = new PlayerTwoProcessor(controller.getPlayers().get(1), actionSubject);
        player1Processor.keyDown(Keys.RIGHT);
        player2Processor.keyDown(Keys.A);
        //verify that the method move of the ActionObserver is being called one time with the right Player and the Right Direction as arguments
        verify(actionObserver, times(1)).move(controller.getPlayers().get(0), Direction.RIGHT);
        verify(actionObserver, times(1)).move(controller.getPlayers().get(1), Direction.LEFT);
        //press the specular and so, not valid, key for the same move
        player1Processor.keyDown(Keys.D);
        player2Processor.keyDown(Keys.RIGHT);
        //verify that previous invalid keys were ignored and move methods have only been called once
        verify(actionObserver, times(1)).move(controller.getPlayers().get(0), Direction.RIGHT);
        verify(actionObserver, times(1)).move(controller.getPlayers().get(1), Direction.LEFT);
    }
    
    /**
     * {@link AssetManagerDecoratorImpl} test method. It tries to pull up a possible asset, loads it and then retrieve the asset from the manager.
     */
    @Test
    public void testAssets() {
        //test if the assets exist
        assertTrue(Gdx.files.internal(".."+File.separator+"core"+File.separator+"assets"+File.separator+SoundManager.EAT.getPath()).exists());
        AssetManagerDecoratorImpl assetManager = new AssetManagerDecoratorImpl();
        //loads the assets to the manager
        assetManager.loadSound(SoundManager.EAT);
        assetManager.finishLoading();
        //the manager successfully loads the assets
        assertTrue(assetManager.isFinished());
        assertTrue(assetManager.isLoaded(SoundManager.EAT.getPath(), Sound.class));
        //the manager successfully provide the loaded assets
        assetManager.get(SoundManager.EAT.getPath(), Sound.class);
        // the asset is unloaded
        assetManager.unload(SoundManager.EAT.getPath());
        assertFalse(assetManager.isLoaded(SoundManager.EAT.getPath(), Sound.class));
    }
}
