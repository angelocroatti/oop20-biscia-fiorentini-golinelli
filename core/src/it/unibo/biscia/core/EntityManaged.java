package it.unibo.biscia.core;

import java.util.List;
import java.util.Optional;

/**
 * Entity with additional method from internal management.
 *
 */
interface EntityManaged extends Entity {

    /**
     * FOr obtain List of cell with smart capacity.
     * 
     * @return Listof smart cells on body
     */
    List<SmartCell> getSmartCells();

    /**
     * remove a single cell from entity.
     * 
     * @param index the index of cell to remove
     * @return true if success, else false
     */
    boolean removeCell(int index);

    /**
     * remove all cells from index to end.
     * 
     * @param index index of first cell to remove
     * @return number of cells removed
     */
    int removeFromCell(int index);

    /**
     * add to Entity methods for perform movements.
     *
     */
    interface Movable extends EntityManaged {
        /**
         * calling for move object.
         * 
         * @return true if movement is performed
         */
        boolean move();

        /**
         * get the direction for next movement, the direction where "look".
         * 
         * @return the direction to move at next moving
         */
        Optional<Direction> getDirection();

        /**
         * add to Movable methods for set direction of movement. This is a hook for the
         * player's controls
         */
        interface Directable extends EntityManaged.Movable {
            void setDirection(Optional<Direction> direction);
        }

    }

    /**
     * add to Entity ability for eat food and grow with energy gained.
     *
     */
    interface Eater extends EntityManaged {
        int INCREMENT_FOR_ENERGY = 3;

        /**
         * calling for eat energy food.
         * 
         * @param energy energy supplies from food,
         */
        void eat(int energy);

        /**
         * request for entity for increase number of cell.
         * 
         * @return true in entity has grown else false
         */
        boolean grow();
    }

    /**
     * add to the entity the amount of energy it provides if it is eaten.
     *
     */
    interface Eatable extends EntityManaged {
        int MAX_ENERGY = 9;

        /**
         * The energy gained eating the food.
         * 
         * @return amount of energy supplied
         */
        int getEnergy();
    }

}
