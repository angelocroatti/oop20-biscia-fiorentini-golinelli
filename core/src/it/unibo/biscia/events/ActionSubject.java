package it.unibo.biscia.events;

/**
 * for register and remove observer of player actions.
 */
public interface ActionSubject extends GenericObserverCollector<ActionObserver> {

}
