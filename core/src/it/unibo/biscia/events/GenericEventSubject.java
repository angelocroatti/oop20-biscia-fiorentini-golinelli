package it.unibo.biscia.events;

import java.util.function.Consumer;

/**
 * a notifier of events to generic observers.
 * 
 * @param <T> the listeners of events
 */
public interface GenericEventSubject<T> extends GenericObserverCollector<T> {

    /**
     * perform an action to any listener registered.
     * 
     * @param event the action to perform
     */
    void notify(Consumer<T> event);

}
