package it.unibo.biscia.events;

import it.unibo.biscia.core.Entity;
import it.unibo.biscia.core.Level;
import it.unibo.biscia.core.Player;

import java.util.List;

/**
 * event listener for general state of game.
 *
 */
public interface StateObserver {

    /**
     * fired when the game is over. Before this event game go in pause without
     * notification.
     */
    void gameOver();

    /**
     * fired when the game goto pause.
     */
    void gamePause();

    /**
     * fired when the game is resumed from pause.
     */
    void gameResume();

    /**
     * fired when starts new level. Before this events the game go in pause without
     * notification. This event is fired also on start of first level
     * 
     * @param level the new level
     */
    void newLevel(Level level);

    /**
     * fired when some entities moved or changed cells during game.
     * 
     * @param entities the subject of event
     */
    void update(List<Entity> entities);

    /**
     * fired when some entities removed during game.
     * 
     * @param entities list of entities removed
     */
    void remove(List<Entity> entities);

    /**
     * fired when some entities added during game.
     * 
     * @param entities list of entities added
     */
    void add(List<Entity> entities);

    /**
     * fired when player lives or point is changed.
     * 
     * @param player player to change
     */
    void updatePlayer(Player player);

}
