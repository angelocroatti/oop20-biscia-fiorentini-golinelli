package it.unibo.biscia.view.actors.ui;

import it.unibo.biscia.view.utils.Actionable;

import java.util.Objects;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * An {@link OverLabel} with an action executed when the user press Enter or
 * Space key or Touches this {@link Label} with the mouse.
 * 
 * @see OverLabel
 * @see Actionable
 * @see InputListener
 *
 */
public class ActionOverLabel extends OverLabel implements Actionable {
    private Runnable action;

    /**
     * @param text   the initial label's text
     * @param skin   {@link skin} of the label
     * @param action the initial action to perform.
     */
    public ActionOverLabel(final CharSequence text, final Skin skin, final Runnable action) {
        super(text, skin);
        this.setAction(action);
        this.addListener(new InputListener() {
            @Override
            public boolean keyDown(final InputEvent event, final int keycode) {
                if (keycode == Keys.SPACE || keycode == Keys.ENTER) {
                    action.run();
                }
                return super.keyDown(event, keycode);
            }

            @Override
            public boolean touchDown(final InputEvent event, final float x, final float y, final int pointer,
                    final int button) {
                action.run();
                return super.touchDown(event, x, y, pointer, button);
            }
        });
    }

    @Override
    public final Runnable getAction() {
        return action;
    }

    @Override
    public final void setAction(final Runnable action) {
        this.action = Objects.requireNonNull(action);
    }
}
