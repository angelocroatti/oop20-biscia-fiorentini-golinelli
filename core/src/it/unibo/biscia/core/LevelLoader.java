package it.unibo.biscia.core;

interface LevelLoader {
    enum WallType {
        WALL, AREA;
    }

    LevelManaged getFirstLevel();

    LevelManaged getLevel(int cardinal);

    LevelManaged getNextLevel(Level level);
}
