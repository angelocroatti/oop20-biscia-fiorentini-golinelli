package it.unibo.biscia.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;
import org.junit.runner.RunWith;

import it.unibo.biscia.core.Controller;
import it.unibo.biscia.utils.fileio.FileIO;
import it.unibo.biscia.utils.fileio.FileIOImpl;

/**
 * Automated test for the Utils package.
 * 
 */
@RunWith(GdxTestRunner.class)
public class TestUtils {
    
    /**
     * {@link FileIO} test method. It tests all primary functionalities of FileIO API.
     */
    @Test
    public void testFileIO() {
        FileIO fileIO = new FileIOImpl("test");
        //Adding the pair key -> value: "boolean" -> true
        fileIO.add("boolean", true);
        //I get the value of key "boolean" and I actually get a true boolean value
        assertTrue(fileIO.get("boolean", Boolean.class).getSecond());
        //I add "speed"-> COntroller.Speed.SPEED5
        fileIO.add("speed", Controller.Speed.SPEED5);
        //Exception is thrown if I try to get a value of an absent key
        assertThrows(IllegalArgumentException.class, () -> fileIO.get("InvalidKey", Controller.Speed.class).getSecond());
        //I actually get the value SPEED% of the Controller.Speed Enum
        assertEquals(Controller.Speed.SPEED5, fileIO.get("speed", Controller.Speed.class).getSecond());
        //I get a default value even if the key is absent
        assertEquals("ValidValue", fileIO.getOrDefaultValue("InvalidKey", String.class, "ValidValue").getSecond());
        //Writing out the file
        fileIO.build();
        //Check if the file actually exists and is not empty
        File test = new File(System.getProperty("user.home")+File.separator+".prefs"+File.separator+"test");
        assertTrue(test.exists() && test.length() != 0);
    }

}
