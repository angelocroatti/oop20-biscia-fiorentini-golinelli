package it.unibo.biscia.view.screens;

import it.unibo.biscia.Biscia;
import it.unibo.biscia.view.managers.AssetManagerDecorator;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import de.golfgl.gdx.controllers.ControllerMenuStage;

/**
 * Every Screen in the application will extend from this class. This interface
 * defines an Abstract class cause the {@link Screen#show()} will be left to
 * implement to the subclasses.
 *
 * @see com.badlogic.gdx.Screen Screen
 */
public interface AbstractScreen {

    /**
     * get Biscia instance. Every AbstractScreen should have a reference to the
     * Biscia main class.
     * 
     * @see Biscia
     * 
     * @return The Biscia instance
     */
    Biscia getBiscia();

    /**
     * Get the AssetmanagerDecorator instance. Every AbstractScreen should have a
     * reference to the common AssetManagerDecorator
     * 
     * @see AssetManagerDecorator
     * 
     * @return An AssetManagerDecorator
     * 
     */
    AssetManagerDecorator getManager();

    /**
     * A ControllerMenuStage instance. Every AbstractScreen must implement a
     * ControllerMenuStage and provide it.
     * 
     * @see ControllerMenuStage
     * 
     * @return The ControllerMenuStage instance
     */
    ControllerMenuStage getStage();

    /**
     * Get the Skin instance. Every AbstractScreen must implement a common Skin and
     * provide it.
     * 
     * @see Skin
     * 
     * @return The Skin instance.
     */
    Skin getSkin();

    /**
     * Get the InputMultiplexer. Every AbstractScreen must implement an
     * InputMUltiplexer used to filter the mouse inputs.
     * 
     * @see InputMultiplexer
     * @see AbstractScreenImpl
     * 
     * @return The InputMultiplexerInstance
     */
    InputMultiplexer getInputMultiplexer();

}
