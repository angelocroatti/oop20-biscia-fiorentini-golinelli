package it.unibo.biscia.tests;

import static org.junit.Assert.assertEquals;


import org.junit.Test;

import it.unibo.biscia.events.GenericEventSubjectImpl;

/**
 * Test for public interfaces and classes of package it.unibo.biscia.events.
 *
 */




public class TestEvents {
	
	private class TestNumber {
		int number;
		public TestNumber(int number) {
			this.number = number;
		}
		
		public void add(int number) {
			System.out.println("add " + number + " to " + this);
			this.number += number; 
		}

		public int getNumber() {
			return this.number; 
		}
		public String toString() {
			return String.valueOf(this.number);
		}
	}
	public int notify;
	@Test
	public void testGenericEventSubjectImpl() {
		var intS = new GenericEventSubjectImpl<TestNumber>();
		var myL1 = new TestNumber(0);
		var myL2 = new TestNumber(100);
		int n1 = myL1.getNumber();
		int n2 = myL2.getNumber();
		//verify notify to all attached observer
		intS.attach(myL1);
		intS.attach(myL2);
		int countNotify = 0;
		for(int i=0; i<100; i++) {
			n1++;
			n2++;
			countNotify = countNotify +2;
			intS.notify(n -> {
				System.out.println("Before calling " + n);
				n.add(1);
				notify++;
				System.out.println("After calling " + n);
			});
			//System.out.println(notify + " " + countNotify);
			assertEquals("notify count wrong", countNotify, notify);
			assertEquals("not changed L1", n1, myL1.getNumber());
			assertEquals("not changed L2", n2, myL2.getNumber());
			
		}
		
		//verify that detached observer do no receive events
		intS.detach(myL2);
		n1--;
		intS.notify(n -> n.add(-1));
		assertEquals("not changed L1 at second turn, detach touched wrong observer ", n1, myL1.getNumber());
		assertEquals("changed L2, detach not removed correct observer ", n2, myL2.getNumber());
		intS.detach(myL1);
		assertEquals("changed L1, detach not removed last observer ", n1, myL1.getNumber());
		assertEquals("changed at second turn L2, is impossible, an observer is added by ghost", n2, myL2.getNumber());
	}	
}
