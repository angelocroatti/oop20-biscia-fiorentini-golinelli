package it.unibo.biscia.core;

import java.util.Set;

interface SmartCell extends Cell {

    /**
     * return a cell to side of current cell indicated.
     * 
     * @param direction side to request
     * @return e cell requested
     */
    SmartCell geSideCell(Direction direction);

    /**
     * return the entities contained on cell.
     * 
     * @return set of entities with this cell on his body
     */
    Set<EntityManaged> getContainedEntities();
}
