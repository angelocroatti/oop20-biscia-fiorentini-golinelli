package it.unibo.biscia.core;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

final class JudgeImpl implements Judge {
    private final Set<PlayerManaged> playersUpdates = new HashSet<>();
    private final Set<Entity> removed = new HashSet<>();
    private final Set<Entity> updated = new HashSet<>();
    private GameState state = GameState.CONTINUE;
    private boolean addFood;

    private void resetChanges() {
        this.removed.clear();
        this.updated.clear();
        this.playersUpdates.clear();
        this.state = GameState.CONTINUE;
        addFood = false;
    }

    private Set<PlayerManaged> getPlayerEat(final List<PlayerManaged> players, final Map<Entity, Integer> eat) {
        return players.stream().filter(p -> eat.keySet().contains(p.getEntity()))
                .peek(p -> p.addPoints(eat.get(p.getEntity()) * Player.POINTS_FOR_FOOD_ENERGY))
                .collect(Collectors.toSet());
    }

    private Set<PlayerManaged> getPlayerDead(final List<PlayerManaged> players, final Set<Entity> dead) {
        return players.stream().filter(p -> dead.contains(p.getEntity())).peek(p -> p.dead())
                .collect(Collectors.toSet());
    }

    private Set<PlayerManaged> getPlayerTrimmed(final List<PlayerManaged> players, final Map<Entity, Integer> trimmed) {
        return players.stream().filter(p -> trimmed.keySet().contains(p.getEntity()))
                .peek(p -> p.addPoints(trimmed.get(p.getEntity()) * Player.POINTS_FOR_FOOD_ENERGY * -1))
                .collect(Collectors.toSet());
    }

    @Override
    public void judges(final int maxFoodCreatedEnergy, final LevelManaged level, final List<PlayerManaged> players,
            final Map<Entity, Integer> eat, final Map<Entity, Integer> trimmed, final Set<Entity> removed,
            final Set<Entity> updated) {
        resetChanges();
        Set<PlayerManaged> mod;
        mod = this.getPlayerEat(players, eat);
        boolean eated = !mod.isEmpty();
        if (eated) {
            playersUpdates.addAll(mod);
        }
        mod = this.getPlayerDead(players, removed);
        final boolean death = !mod.isEmpty();
        if (death) {
            playersUpdates.addAll(mod);
        }
        playersUpdates.addAll(this.getPlayerTrimmed(players, trimmed));
        if (!eated && Levels.getEntityOfType(level, EntityType.FOOD).isEmpty()) {
            eated = true;
        }
        // non è successo niente (non morto non mangiato)
        // prosegui (non morto, mangiato ma non finito il cibo)
        // reset livello (morto ma non finite le vite)
        // nuovo livello (non morto, mangiato e finito il cibo)
        // gameover (morto e finite le vite)
        if (!(eated || death)) {
            this.removed.addAll(removed);
            this.updated.addAll(updated);
        } else {
            if (!death) {
                this.removed.addAll(removed);
                this.updated.addAll(updated);
                if (Levels.getEatables(level).isEmpty()) {
                    if (maxFoodCreatedEnergy < EntityManaged.Eatable.MAX_ENERGY) {
                        // proseguo
                        this.addFood = true;
                    } else {
                        // nuovo livello
                        this.state = GameState.NEXT_LEVEL;
                    }
                }
            } else {
                if (players.stream().filter(p -> p.getLives() == 0).findAny().isPresent()) {
                    // gameover
                    this.state = GameState.GAMEOVER;
                    this.updated.addAll(updated);
                } else {
                    // ricomincio il livello
                    this.state = GameState.RESTART_LEVEL;
                }
            }
        }
    }

    @Override
    public Set<Entity> getRemoved() {
        return Collections.unmodifiableSet(this.removed);
    }

    @Override
    public Set<Entity> getUpdated() {
        return Collections.unmodifiableSet(this.updated);
    }

    @Override
    public Set<PlayerManaged> getPlayersUpdates() {
        return Collections.unmodifiableSet(this.playersUpdates);
    }

    @Override
    public GameState getState() {
        return this.state;
    }

    /**
     * true if the level continues and does not contain food.
     * 
     * @return true if level need new food
     */
    @Override
    public boolean addFood() {
        if (this.getState().equals(GameState.CONTINUE)) {
            return this.addFood;
        }
        return false;
    }
}
