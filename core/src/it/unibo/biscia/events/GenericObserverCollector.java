package it.unibo.biscia.events;

/**
 * for register and remove observer of events.
 *
 * @param <T> a type of observer
 */
public interface GenericObserverCollector<T> {

    /**
     * add listener.
     * 
     * @param observer subject to notify
     */
    void attach(T observer);

    /**
     * remove listener.
     * 
     * @param observer subject to notify
     */

    void detach(T observer);

}
